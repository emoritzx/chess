package me.abiogenesis.chess.cli.view;

import me.abiogenesis.chess.engine.BasicEventHandler;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.Placement;

public class PlaceEventLog extends BasicEventHandler<Placement> {

    public PlaceEventLog() {
        super(Placement.class);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Placement event) {
        System.out.printf("Placed (%c) %s on (%d, %d)%n",
            event.getPiece().getColor().name().charAt(0),
            event.getPiece().getName(),
            event.getPosition().getColumn(),
            event.getPosition().getRow());
    }
}
