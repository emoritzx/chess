package me.abiogenesis.chess.cli.view;

import me.abiogenesis.chess.engine.BasicEventHandler;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.Removal;

public class RemoveEventLog extends BasicEventHandler<Removal> {

    public RemoveEventLog() {
        super(Removal.class);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Removal event) {
        System.out.printf("Removed (%c) %s from (%d, %d)%n",
            event.getPiece().getColor().name().charAt(0),
            event.getPiece().getName(),
            event.getPosition().getColumn(),
            event.getPosition().getRow());
    }
}
