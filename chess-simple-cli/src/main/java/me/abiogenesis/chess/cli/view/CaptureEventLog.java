package me.abiogenesis.chess.cli.view;

import me.abiogenesis.chess.engine.BasicEventHandler;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.Capture;

public class CaptureEventLog extends BasicEventHandler<Capture> {

    public CaptureEventLog() {
        super(Capture.class);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Capture event) {
        System.out.printf("(%c) %s captured (%c) %s at (%d, %d)%n",
            event.getAttacker().getColor().name().charAt(0),
            event.getAttacker().getName(),
            event.getDefender().getColor().name().charAt(0),
            event.getDefender().getName(),
            event.getPosition().getColumn(),
            event.getPosition().getRow());
    }
}
