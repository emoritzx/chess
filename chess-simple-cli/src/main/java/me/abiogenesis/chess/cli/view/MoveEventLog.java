package me.abiogenesis.chess.cli.view;

import me.abiogenesis.chess.engine.BasicEventHandler;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.Move;

public class MoveEventLog extends BasicEventHandler<Move> {

    public MoveEventLog() {
        super(Move.class);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Move event) {
        System.out.printf("Move (%c) %s (%d, %d) -> (%d, %d)%n",
            event.getPiece().getColor().name().charAt(0),
            event.getPiece().getName(),
            event.getMove().getStart().getColumn(),
            event.getMove().getStart().getRow(),
            event.getMove().getEnd().getColumn(),
            event.getMove().getEnd().getRow());
    }
}
