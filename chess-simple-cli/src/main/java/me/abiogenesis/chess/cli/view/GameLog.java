package me.abiogenesis.chess.cli.view;


import me.abiogenesis.chess.engine.ChessEventHandler;
import me.abiogenesis.chess.engine.GameEngine;

import java.util.Arrays;
import java.util.List;

public class GameLog {

    public static void setup(GameEngine engine) {
        List<ChessEventHandler<?>> loggers = Arrays.asList(
            new MoveEventLog(),
            new PlaceEventLog(),
            new RemoveEventLog(),
            new CaptureEventLog()
        );
        loggers.stream()
            .forEach(engine::register);
    }
}
