package me.abiogenesis.chess.cli;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.Chess;
import me.abiogenesis.chess.cli.view.GameLog;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.Move;
import me.abiogenesis.chess.events.Placement;
import me.abiogenesis.chess.movement.Position;
import me.abiogenesis.chess.movement.Translation;
import me.abiogenesis.chess.pieces.Pawn;

import java.util.Collection;

import static me.abiogenesis.chess.pieces.Color.BLACK;
import static me.abiogenesis.chess.pieces.Color.WHITE;

public class Main {

    public static void main(String[] args) throws Exception {

        Board board = new Board(8);
        GameEngine engine = new GameEngine();
        Chess.configure(engine, board);
        GameLog.setup(engine);

        // events
        Pawn whitePawn = new Pawn(WHITE);
        engine.process(new Placement(new Pawn(BLACK), new Position(8, 1)));
        engine.process(new Placement(new Pawn(BLACK), new Position(7, 1)));
        Position position = new Position(8, 4);
        engine.process(new Placement(whitePawn, position));
        Collection<Translation> moves = whitePawn.getMovement().getMoves(board.getView(), position);
        while (!moves.isEmpty()) {
            Translation translation = moves.iterator().next();
            engine.process(new Move(whitePawn, translation));
            position = translation.getEnd();
            moves = whitePawn.getMovement().getMoves(board.getView(), position);
        }
    }
}
