package me.abiogenesis.chess.pieces;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.movement.Position;
import me.abiogenesis.chess.movement.Translation;
import org.apache.commons.collections4.CollectionUtils;
import org.testng.annotations.Test;

import java.util.*;

import static me.abiogenesis.chess.pieces.Color.WHITE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class KingMovementTest {

    @Test
    public void testCompleteRange() {
        Position startingPosition = new Position(3, 3);
        Set<Position> validPositions = new LinkedHashSet<>();
        for (int dx : Arrays.asList(-1, 0, 1)) {
            for (int dy : Arrays.asList(-1, 0, 1)) {
                if (!(dx == 0 && dy == 0)) {
                    validPositions.add(new Position(startingPosition.getRow() + dx, startingPosition.getColumn() + dy));
                }
            }
        }
        King king = new King(WHITE);
        Collection<Translation> actualTranslations = king.getMovement().getMoves(new Board(8).getView(), startingPosition);
        List<Position> endPositions = new ArrayList<>();
        for (Translation translation : actualTranslations) {
            assertEquals(translation.getStart(), startingPosition);
            endPositions.add(translation.getEnd());
        }
        assertTrue(CollectionUtils.isEqualCollection(validPositions, endPositions), "End position");
    }
}
