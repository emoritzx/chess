package me.abiogenesis.chess;

import me.abiogenesis.chess.movement.Position;
import me.abiogenesis.chess.pieces.Piece;

import java.util.Collection;
import java.util.LinkedList;

public class Board {

    private final Collection<Piece>[][] board;

    public Board(int size) {
        board = new Collection[size][size];
        for (int x = 0; x < size; ++x) {
            for (int y = 0; y < size; ++y) {
                board[x][y] = new LinkedList<>();
            }
        }
    }

    public View getView() {
        return new View();
    }

    public void place(Piece piece, Position position) {
        getView().query(position)
            .add(piece);
    }

    public void remove(Piece piece, Position position) {
        getView().query(position)
            .remove(piece);
    }

    public class View {

        public Collection<Piece> query(Position position) {
            int row = translateRow(position);
            int column = translateColumn(position);
            return board[row][column];
        }

        private int translateColumn(Position position) {
            return position.getColumn() - 1;
        }

        private int translateRow(Position position) {
            return position.getRow() - 1;
        }

        public int getSize() {
            return board.length;
        }
    }
}
