package me.abiogenesis.chess.exceptions;

import me.abiogenesis.chess.movement.Position;

public class TooManyPiecesInPositionException extends PositionException {

    public TooManyPiecesInPositionException(Position position) {
        super(position);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ": Too many pieces at position.";
    }
}
