package me.abiogenesis.chess.exceptions;

import me.abiogenesis.chess.movement.Position;

public class PositionException extends ChessException {

    private final Position position;

    public PositionException(Position position) {
        super("Error at position: " + position.toString());
        this.position = position;
    }

    public final Position getPosition() {
        return position;
    }
}
