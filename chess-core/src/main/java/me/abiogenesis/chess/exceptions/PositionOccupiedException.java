package me.abiogenesis.chess.exceptions;

import me.abiogenesis.chess.movement.Position;

public class PositionOccupiedException extends PositionException {

    public PositionOccupiedException(Position position) {
        super(position);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ": Position is occupied.";
    }
}
