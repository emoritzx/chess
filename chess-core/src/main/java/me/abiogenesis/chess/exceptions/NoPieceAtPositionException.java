package me.abiogenesis.chess.exceptions;

import me.abiogenesis.chess.movement.Position;

public class NoPieceAtPositionException extends PositionException {

    public NoPieceAtPositionException(Position position) {
        super(position);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + ": No piece at position.";
    }
}
