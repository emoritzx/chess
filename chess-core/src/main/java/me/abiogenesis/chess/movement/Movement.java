package me.abiogenesis.chess.movement;

import me.abiogenesis.chess.Board;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Movement {

    Collection<Translation> getMoves(Board.View board, Position position);

    default Movement combine(Movement other) {
        return (board, position) ->
            Stream.concat(
                    getMoves(board, position).stream(),
                    other.getMoves(board, position).stream())
                .collect(Collectors.toList());
    }

    static Movement combineAll(Movement ... movements) {
        return Arrays.stream(movements)
            .reduce(Movement::combine)
            .orElseThrow(() -> new IllegalArgumentException("Need at least 1 argument"));
    }
}
