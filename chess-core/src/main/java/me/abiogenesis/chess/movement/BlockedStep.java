package me.abiogenesis.chess.movement;

import me.abiogenesis.chess.Board;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BlockedStep implements Movement {

    private final int x;
    private final int y;

    public BlockedStep(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public Collection<Translation> getMoves(Board.View board, Position position) {
        Position target = position.translate(x, y);
        return inBounds(board, target) && pathIsClear(board, position, target)
            ? Collections.singleton(new Translation(position, target))
            : Collections.emptySet();
    }

    private boolean inBounds(Board.View board, Position target) {
        return target.getColumn() >= 1 && target.getColumn() <= board.getSize()
            && target.getRow() >= 1 && target.getRow() <= board.getSize();
    }

    private boolean pathIsClear(Board.View board, Position start, Position end) {
        return generatePositions(start, end).stream()
            .allMatch(position -> positionIsClear(board, position));
    }

    private Collection<Position> generatePositions(Position start, Position end) {
        int dy = end.getRow() - start.getRow();
        int dx = end.getColumn() - start.getColumn();
        int verticalDirection = Integer.signum(dy);
        int horizontalDirection = Integer.signum(dx);
        Position currentPosition = start;
        List<Position> positions = new LinkedList<>();
        do {
            currentPosition = currentPosition.translate(horizontalDirection, verticalDirection);
            positions.add(currentPosition);
        } while (!currentPosition.equals(end));
        return positions;
    }

    private boolean positionIsClear(Board.View board, Position position) {
        return board.query(position).isEmpty();
    }
}
