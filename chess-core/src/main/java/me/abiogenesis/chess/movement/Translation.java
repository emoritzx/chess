package me.abiogenesis.chess.movement;

import java.util.Objects;

public class Translation {

    private final Position start;
    private final Position end;

    public Translation(Position start, Position end) {
        this.start = Objects.requireNonNull(start);
        this.end = Objects.requireNonNull(end);
    }

    public Position getStart() {
        return start;
    }

    public Position getEnd() {
        return end;
    }
}
