package me.abiogenesis.chess.movement;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.pieces.Pawn;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ConditionalDiagonalCapturingStep implements Movement {

    private final Pawn piece;

    public ConditionalDiagonalCapturingStep(Pawn piece) {
        this.piece = piece;
    }

    @Override
    public Collection<Translation> getMoves(Board.View board, Position position) {
        CapturingStep left = new CapturingStep(piece, -1, piece.getDirection());
        CapturingStep right = new CapturingStep(piece, 1, piece.getDirection());
        Position leftTarget = position.translate(left.getY(), left.getX());
        Position rightTarget = position.translate(right.getY(), right.getX());
        List<Translation> moves = new LinkedList<>();
        analyze(board, position, leftTarget, moves);
        analyze(board, position, rightTarget, moves);
        return moves;
    }

    private void analyze(Board.View board, Position start, Position target, List<Translation> moves) {
        if (inBounds(board, target) && canCapture(board, target)) {
            moves.add(new Translation(start, target));
        }
    }

    private boolean inBounds(Board.View board, Position target) {
        return target.getColumn() >= 1 && target.getColumn() <= board.getSize()
            && target.getRow() >= 1 && target.getRow() <= board.getSize();
    }

    private boolean canCapture(Board.View board, Position target) {
        return board.query(target).stream()
            .anyMatch(targetPiece -> targetPiece.isOpponent(piece));
    }
}
