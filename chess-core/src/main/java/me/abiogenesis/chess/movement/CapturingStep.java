package me.abiogenesis.chess.movement;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.pieces.Piece;

import java.util.Collection;
import java.util.Collections;

public class CapturingStep implements Movement {

    private final Piece piece;
    private final int x;
    private final int y;

    public CapturingStep(Piece piece, int x, int y) {
        this.piece = piece;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public Collection<Translation> getMoves(Board.View board, Position position) {
        Position target = position.translate(x, y);
        return targetIsValid(board, target)
            ? Collections.singleton(new Translation(position, target))
            : Collections.emptySet();
    }

    private boolean targetIsValid(Board.View board, Position target) {
        return inBounds(board, target) && (isEmpty(board, target) || hasOpponent(board, target));
    }

    private boolean inBounds(Board.View board, Position target) {
        return target.getColumn() >= 1 && target.getColumn() <= board.getSize()
            && target.getRow() >= 1 && target.getRow() <= board.getSize();
    }

    private boolean isEmpty(Board.View board, Position target) {
        return board.query(target)
            .isEmpty();
    }

    private boolean hasOpponent(Board.View board, Position target) {
        return board.query(target).stream()
            .anyMatch(targetPiece -> piece.isOpponent(targetPiece));
    }
}
