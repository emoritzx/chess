package me.abiogenesis.chess.events;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.engine.BoardInteraction;
import me.abiogenesis.chess.engine.GameEngine;

public class RemovalHandler extends BoardInteraction<Removal> {

    public RemovalHandler(Board board) {
        super(Removal.class, board);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Removal event) {
        board.remove(event.getPiece(), event.getPosition());
    }
}
