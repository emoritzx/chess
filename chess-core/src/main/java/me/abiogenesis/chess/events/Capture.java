package me.abiogenesis.chess.events;

import me.abiogenesis.chess.engine.ChessEvent;
import me.abiogenesis.chess.movement.Position;
import me.abiogenesis.chess.pieces.Piece;

public class Capture extends ChessEvent {

    private final Piece attacker;
    private final Piece defender;
    private final Position position;

    public Capture(Piece attacker, Piece defender, Position position) {
        this.attacker = attacker;
        this.defender = defender;
        this.position = position;
    }

    public Piece getAttacker() {
        return attacker;
    }

    public Piece getDefender() {
        return defender;
    }

    public Position getPosition() {
        return position;
    }
}
