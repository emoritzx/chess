package me.abiogenesis.chess.events;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.engine.InterimEventHandler;

public class CaptureHandler extends InterimEventHandler<Capture> {

    public CaptureHandler(Board.View board) {
        super(Capture.class, board);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Capture event) {
        block.add(new Removal(event.getDefender(), event.getPosition()));
    }
}
