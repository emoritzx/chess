package me.abiogenesis.chess.events;

import me.abiogenesis.chess.engine.ChessEvent;
import me.abiogenesis.chess.movement.Position;
import me.abiogenesis.chess.pieces.Piece;

public class Removal extends ChessEvent {

    private final Piece piece;
    private final Position position;

    public Removal(Piece piece, Position position) {
        this.piece = piece;
        this.position = position;
    }

    public Piece getPiece() {
        return piece;
    }

    public Position getPosition() {
        return position;
    }
}
