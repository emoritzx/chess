package me.abiogenesis.chess.events;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.engine.BoardInteraction;
import me.abiogenesis.chess.engine.GameEngine;

public class PlacementHandler extends BoardInteraction<Placement> {

    public PlacementHandler(Board board) {
        super(Placement.class, board);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Placement event) {
        board.place(event.getPiece(), event.getPosition());
    }
}
