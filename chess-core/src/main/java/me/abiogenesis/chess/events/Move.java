package me.abiogenesis.chess.events;

import me.abiogenesis.chess.engine.ChessEvent;
import me.abiogenesis.chess.movement.Translation;
import me.abiogenesis.chess.pieces.Piece;

public class Move extends ChessEvent {

    private final Piece piece;
    private final Translation translation;

    public Move(Piece piece, Translation translation) {
        this.piece = piece;
        this.translation = translation;
    }

    public Piece getPiece() {
        return piece;
    }

    public Translation getMove() {
        return translation;
    }
}
