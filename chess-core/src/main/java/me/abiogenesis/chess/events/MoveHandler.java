package me.abiogenesis.chess.events;

import me.abiogenesis.chess.Board;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.engine.InterimEventHandler;
import me.abiogenesis.chess.movement.Translation;
import me.abiogenesis.chess.pieces.Piece;

public class MoveHandler extends InterimEventHandler<Move> {

    public MoveHandler(Board.View board) {
        super(Move.class, board);
    }

    @Override
    public void handle(GameEngine.ProcessBlock block, Move event) {
        Piece attacker = event.getPiece();
        Translation move = event.getMove();
        block.add(new Removal(attacker, move.getStart()));
        block.add(new Placement(attacker, move.getEnd()));
        board.query(event.getMove().getEnd()).stream()
            .filter(defender -> defender.isOpponent(attacker))
            .forEachOrdered(defender -> block.add(new Capture(attacker, defender, move.getEnd())));
    }
}
