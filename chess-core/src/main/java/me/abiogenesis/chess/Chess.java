package me.abiogenesis.chess;

import me.abiogenesis.chess.engine.ChessEventHandler;
import me.abiogenesis.chess.engine.GameEngine;
import me.abiogenesis.chess.events.CaptureHandler;
import me.abiogenesis.chess.events.MoveHandler;
import me.abiogenesis.chess.events.PlacementHandler;
import me.abiogenesis.chess.events.RemovalHandler;

import java.util.Arrays;
import java.util.List;

public class Chess {

    public static GameEngine newGame(int boardSize) {
        GameEngine engine = new GameEngine();
        configure(engine, new Board(boardSize));
        return engine;
    }

    public static void configure(GameEngine engine, Board board) {;
        Board.View view = board.getView();
        List<ChessEventHandler<?>> handlers = Arrays.asList(
            new CaptureHandler(view),
            new MoveHandler(view),
            new PlacementHandler(board),
            new RemovalHandler(board));
        handlers.forEach(engine::register);
    }
}
