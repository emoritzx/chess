package me.abiogenesis.chess.engine;

public interface ChessEventHandler<EventType extends ChessEvent> {

    void handle(GameEngine.ProcessBlock block, EventType event);

    Class<EventType> getEventType();

    default boolean canHandle(Class<? extends ChessEvent> otherEventType) {
        return otherEventType.isAssignableFrom(getEventType());
    }
}
