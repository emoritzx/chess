package me.abiogenesis.chess.engine;

import me.abiogenesis.chess.Board;

public abstract class InterimEventHandler<EventType extends ChessEvent> extends BasicEventHandler<EventType> {

    protected final Board.View board;

    public InterimEventHandler(Class<EventType> eventType, Board.View board) {
        super(eventType);
        this.board = board;
    }
}
