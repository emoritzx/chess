package me.abiogenesis.chess.engine;

import me.abiogenesis.chess.Board;

public abstract class BoardInteraction<EventType extends ChessEvent> extends BasicEventHandler<EventType> {

    protected final Board board;

    protected BoardInteraction(Class<EventType> eventType, Board board) {
        super(eventType);
        this.board = board;
    }
}
