package me.abiogenesis.chess.engine;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class GameEngine {

    private Collection<ChessEventHandler> handlers = new LinkedList<>();

    public void register(ChessEventHandler<? extends ChessEvent> handler) {
        handlers.add(handler);
    }

    public void unregister(ChessEventHandler<? extends  ChessEvent> handler) {
        handlers.remove(handler);
    }

    public void process(ChessEvent event) {
        ProcessBlock block = new ProcessBlock();
        handlers.stream()
            .filter(handler -> handler.canHandle(event.getClass()))
            .forEachOrdered(handler -> handler.handle(block, event));
        block.events.forEach(this::process);
    }

    public class ProcessBlock {

        private final List<ChessEvent> events = new LinkedList<>();

        public void add(ChessEvent event) {
            events.add(event);
        }
    }
}
