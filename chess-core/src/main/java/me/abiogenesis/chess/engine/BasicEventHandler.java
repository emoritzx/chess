package me.abiogenesis.chess.engine;

public abstract class BasicEventHandler<EventType extends ChessEvent> implements ChessEventHandler<EventType> {

    private final Class<EventType> eventType;

    public BasicEventHandler(Class<EventType> eventType) {
        this.eventType = eventType;
    }

    @Override
    public final Class<EventType> getEventType() {
        return eventType;
    }
}
