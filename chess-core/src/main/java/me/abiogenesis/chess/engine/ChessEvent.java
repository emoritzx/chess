package me.abiogenesis.chess.engine;

public abstract class ChessEvent {

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
