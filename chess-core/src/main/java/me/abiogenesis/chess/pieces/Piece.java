package me.abiogenesis.chess.pieces;

import me.abiogenesis.chess.movement.Movement;

public interface Piece {

    Color getColor();

    String getName();

    Movement getMovement();

    int getMoveCount();
    void move();

    default boolean isOpponent(Piece other) {
        return other.getColor() != getColor();
    }

    default char getAbbreviation() {
        return getName().charAt(0);
    }
}
