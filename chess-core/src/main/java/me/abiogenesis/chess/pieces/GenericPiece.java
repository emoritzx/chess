package me.abiogenesis.chess.pieces;

import java.util.Objects;

public abstract class GenericPiece implements Piece {

    private int moveCount = 0;
    private final Color color;

    public GenericPiece(Color color) {
        this.color = Objects.requireNonNull(color);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public int getMoveCount() {
        return moveCount;
    }

    @Override
    public void move() {
        ++moveCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericPiece that = (GenericPiece) o;
        return moveCount == that.moveCount &&
            color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(moveCount, color);
    }
}
