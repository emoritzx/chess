package me.abiogenesis.chess.pieces;

import me.abiogenesis.chess.movement.BlockedStep;
import me.abiogenesis.chess.movement.ConditionalDiagonalCapturingStep;
import me.abiogenesis.chess.movement.Movement;

import static me.abiogenesis.chess.pieces.Color.WHITE;

public class Pawn extends GenericPiece {

    public Pawn(Color color) {
        super(color);
    }

    @Override
    public Movement getMovement() {
        Movement movement = Movement.combineAll(
            new BlockedStep(0, getDirection()),
            new ConditionalDiagonalCapturingStep(this));
        if (getMoveCount() == 0) {
            movement.combine(new BlockedStep( 0, 2 * getDirection()));
        }
        return movement;
    }

    public int getDirection() {
        return getColor() == WHITE ? -1 : 1;
    }
}
