package me.abiogenesis.chess.pieces;

public enum Color {
    WHITE,
    BLACK
}
