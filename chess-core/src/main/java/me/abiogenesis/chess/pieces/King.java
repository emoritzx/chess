package me.abiogenesis.chess.pieces;

import me.abiogenesis.chess.movement.Movement;
import me.abiogenesis.chess.movement.CapturingStep;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Predicate;

public class King extends GenericPiece {

    public King(Color color) {
        super(color);
    }

    @Override
    public Movement getMovement() {
        Set<Integer> verticalMovement = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(-1, 0, 1)));
        Set<Integer> horizontalMovement = verticalMovement;
        Predicate<CapturingStep> noMovement = step -> step.getX() == 0 && step.getY() == 0;
        Predicate<CapturingStep> netMovement = noMovement.negate();
        return horizontalMovement.stream()
            .flatMap(x -> verticalMovement.stream()
                .map(y -> new CapturingStep(this, x, y)))
            .filter(netMovement)
            .map(Movement.class::cast)
            .reduce(Movement::combine)
            .orElseThrow(() -> new IllegalStateException("No moves"));
    }
}
